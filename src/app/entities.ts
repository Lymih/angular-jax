export interface Operation {
  id?:number;
  label:string;
  date:Date;
  amount:number;

}