import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Operation } from './entities';

@Injectable({
  providedIn: 'root'
})
export class OperationService {

  getAll(){
    
    return this.http.get<Operation[]>(environment.apiUrl+'/api/operation');
  }

  getById(id:number){
    return this.http.get<Operation>(environment.apiUrl+'/api/operation/'+id);
  }

  add(operation:Operation){
    return this.http.post<Operation>(environment.apiUrl+'/api/operation/',operation);
  }
  delete(id:number){
    return this.http.delete<number>(environment.apiUrl+'/api/operation/'+id);
  }
  put(operation:Operation){
    return this.http.put<Operation>(environment.apiUrl+'/api/operation/',operation);
  }
  
  constructor(private http:HttpClient) { }
}

