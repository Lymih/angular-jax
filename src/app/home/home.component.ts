import { Component, OnInit } from '@angular/core';
import { Operation } from '../entities';
import { OperationService } from '../operation.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  operations:Operation[]=[];
  single?:Operation;
  constructor(private oS:OperationService) { }
  
  fetchOne(id:number){
    this.oS.getById(id).subscribe(data => this.single =data);
  }
 deleteOne(id:number){
   this.oS.delete(id).subscribe();
   this.operations.forEach(operation=>{
    if(operation.id==id) this.operations.splice(this.operations.indexOf(operation,1));
 });
 }
 
  ngOnInit(): void {
   this.oS.getAll().subscribe(data => this.operations = data);

    }

}
