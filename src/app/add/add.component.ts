import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Operation } from '../entities';
import { OperationService } from '../operation.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  operation:Operation = {label:'',amount:0.0, date: new Date()};
  addOperation(){

    this.oS.add(this.operation).subscribe(()=>{this.router.navigate(['/'])})
  }
  constructor(private oS:OperationService, private router:Router) { }

  ngOnInit(): void {
  }

}
